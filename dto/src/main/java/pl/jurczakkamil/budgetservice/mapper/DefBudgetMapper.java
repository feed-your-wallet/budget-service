package pl.jurczakkamil.budgetservice.mapper;

import org.mapstruct.Mapper;
import pl.jurczakkamil.budgetservice.DefBudget;
import pl.jurczakkamil.budgetservice.DefBudgetDto;

@Mapper(componentModel = "spring")
public interface DefBudgetMapper {

    DefBudgetDto toDto(DefBudget defBudget);

    DefBudget toEntity(DefBudgetDto defBudgetDto);
}
