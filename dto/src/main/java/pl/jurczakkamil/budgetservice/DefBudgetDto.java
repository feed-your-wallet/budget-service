package pl.jurczakkamil.budgetservice;

import lombok.*;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DefBudgetDto extends BaseDto {

    private String name;
    private int period;
    private BigDecimal amount;
    private Long[] accountIds = new Long[]{0L};
    private Long[] categoryIds = new Long[]{0L};
}
