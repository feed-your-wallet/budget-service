package pl.jurczakkamil.budgetservice;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public abstract class BaseDto {

    private Long id;
    private Long version;
}
