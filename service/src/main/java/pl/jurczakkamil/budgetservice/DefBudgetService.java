package pl.jurczakkamil.budgetservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jurczakkamil.budgetservice.mapper.DefBudgetMapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;

@Service
public class DefBudgetService {

    private DefBudgetRepository defBudgetRepository;
    private DefBudgetMapper defBudgetMapper;

    @Autowired
    public DefBudgetService(DefBudgetRepository defBudgetRepository, DefBudgetMapper defBudgetMapper) {
        this.defBudgetRepository = defBudgetRepository;
        this.defBudgetMapper = defBudgetMapper;
    }

    public DefBudgetDto save(DefBudgetDto defBudgetDto) {
        final var defBudget = defBudgetMapper.toEntity(defBudgetDto);
        final var saved = defBudgetRepository.save(defBudget);
        return defBudgetMapper.toDto(saved);
    }

    public List<DefBudgetDto> saveAll(List<DefBudgetDto> dtos) {
        final var defBudgets = dtos.stream()
                .map(e -> defBudgetMapper.toEntity(e))
                .toList();
        final var saved = defBudgetRepository.saveAll(defBudgets);
        return StreamSupport.stream(saved.spliterator(), false)
                .map(e -> defBudgetMapper.toDto(e))
                .toList();
    }

    public Optional<DefBudgetDto> findById(Long id) {
        requireNonNull(id, "ID cannot be null");
        final var optionalDefBudget = defBudgetRepository.findById(id);
        if (optionalDefBudget.isEmpty()) return Optional.empty();

        final var defBudget = optionalDefBudget.get();
        final var defBudgetDto = defBudgetMapper.toDto(defBudget);
        return Optional.of(defBudgetDto);
    }

    public List<DefBudgetDto> findAllById(List<Long> ids) {
        final var defBudgets = defBudgetRepository.findAllById(ids);
        return StreamSupport.stream(defBudgets.spliterator(), false)
                .map(e -> defBudgetMapper.toDto(e))
                .toList();
    }

    public List<DefBudgetDto> findAll() {
        final var defBudgets = defBudgetRepository.findAll();
        return StreamSupport.stream(defBudgets.spliterator(), false)
                .map(e -> defBudgetMapper.toDto(e))
                .toList();
    }

    public BigDecimal sumAmountsById(List<Long> ids) {
        final var found = findAllById(ids);
        return found.stream()
                .map(DefBudgetDto::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
