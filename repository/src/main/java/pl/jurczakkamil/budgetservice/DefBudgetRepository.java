package pl.jurczakkamil.budgetservice;

import org.springframework.data.repository.CrudRepository;

public interface DefBudgetRepository extends CrudRepository<DefBudget, Long> {

}
