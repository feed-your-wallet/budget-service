package pl.jurczakkamil.budgetservice;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Component
public class RestApi extends RouteBuilder {

    @Override
    public void configure() {

        restConfiguration()
                .contextPath("{{server.servlet.context-path}}")
                .port("{{server.port}}")
                .enableCORS(true)
                .apiContextPath("{{server.servlet.context-path}}")
                .apiProperty("api.budget-management.title", "{{spring.application.name}}")
                .apiProperty("api.budget-management.version", "{{application.version}}")
                .apiContextRouteId("doc-budget-management-api")
                .component("servlet")
                .bindingMode(RestBindingMode.json);

        rest("/api")
                .get("/hello").consumes(APPLICATION_JSON)
                .to("direct:hello")
                .get("/budgets").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .to("direct:get-all-def-budgets")
                .get("/budgets/{id}").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .to("direct:get-def-budget-by-id")
                .post("/budgets").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .type(DefBudgetDto.class)
                .to("direct:add-def-budget");
    }
}
