package pl.jurczakkamil.budgetservice.http;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Header {

    ID("id");

    private String name;
}
