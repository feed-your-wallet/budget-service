package pl.jurczakkamil.budgetservice.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.budgetservice.DefBudgetDto;
import pl.jurczakkamil.budgetservice.DefBudgetService;

@Component
public class AddDefBudgetRoute extends RouteBuilder {

    private final DefBudgetService defBudgetService;

    @Autowired
    public AddDefBudgetRoute(DefBudgetService defBudgetService) {
        this.defBudgetService = defBudgetService;
    }

    @Override
    public void configure() {
        from("direct:add-def-budget").id("add-definition-of-budget")
                .log(">>> ${body}")
                .process(this::saveBudget)
                .setBody(exchange -> "Successfully added budget.");
    }

    private DefBudgetDto saveBudget(Exchange exchange) {
        final var defBudgetDto = exchange.getIn().getBody(DefBudgetDto.class);
        return defBudgetService.save(defBudgetDto);
    }
}
