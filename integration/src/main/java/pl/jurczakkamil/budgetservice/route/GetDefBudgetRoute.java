package pl.jurczakkamil.budgetservice.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.budgetservice.DefBudgetDto;
import pl.jurczakkamil.budgetservice.DefBudgetService;
import pl.jurczakkamil.budgetservice.http.Header;

@Component
public class GetDefBudgetRoute extends RouteBuilder {

    private final DefBudgetService defBudgetService;

    @Autowired
    public GetDefBudgetRoute(DefBudgetService defBudgetService) {
        this.defBudgetService = defBudgetService;
    }

    @Override
    public void configure() {
        from("direct:get-def-budget-by-id").id("get-definition-of-budget-by-id")
                .setBody(this::findBudget);
    }

    private DefBudgetDto findBudget(Exchange exchange) {
        final var headerName = Header.ID.getName();
        final var id = exchange.getIn().getHeader(headerName, Long.class);
        return defBudgetService.findById(id)
                .orElse(null);
    }
}
