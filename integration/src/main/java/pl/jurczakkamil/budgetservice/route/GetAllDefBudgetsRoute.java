package pl.jurczakkamil.budgetservice.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.budgetservice.DefBudgetService;

@Component
public class GetAllDefBudgetsRoute extends RouteBuilder {

    private final DefBudgetService defBudgetService;

    @Autowired
    public GetAllDefBudgetsRoute(DefBudgetService defBudgetService) {
        this.defBudgetService = defBudgetService;
    }

    @Override
    public void configure() {
        from("direct:get-all-def-budgets").id("get-all-definitions-of-budgets")
                .setBody(exchange -> defBudgetService.findAll());
    }
}
