package pl.jurczakkamil.budgetservice;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import pl.jurczakkamil.budgetservice.type.LongArrayType;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@TypeDef(name = "longArray",
        typeClass = LongArrayType.class)
@Getter
@Setter
@ToString
public class DefBudget extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private int period;

    @Column(nullable = false)
    private BigDecimal amount;

    @Type(type = "longArray")
    @Column(columnDefinition = "bigint[]", nullable = false)
    private Long[] accountIds = new Long[]{0L};

    @Type(type = "longArray")
    @Column(columnDefinition = "bigint[]", nullable = false)
    private Long[] categoryIds = new Long[]{0L};

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        final var defBudget = (DefBudget) o;

        return Objects.equals(getId(), defBudget.getId());
    }

    @Override
    public int hashCode() {
        return 407085047;
    }
}
