package pl.jurczakkamil.budgetservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static pl.jurczakkamil.budgetservice.helper.DefBudgetHelper.createDefBudget;

@SpringBootTest
class DefBudgetRepositoryTests {

    @Autowired
    private DefBudgetRepository defBudgetRepository;

    @Test
    void contextLoads() {
        assertNotNull(defBudgetRepository);
    }

    @Test
    void givenDefBudget_whenSave_thenPersistedOkInDB() {
        final var defBudget = createDefBudget();

        final var saved = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(saved.getId());
        assertTrue(found.isPresent());
    }

    @Test
    void givenDefBudgetWithoutAccounts_whenSave_thenPersistedInDBWithDefault() {
        final var defBudget = createDefBudget(true, true, true, false, true);

        final var saved = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(saved.getId());
        assertTrue(found.isPresent());
        assertEquals(1, found.get().getAccountIds().length);
        assertEquals(0L, found.get().getAccountIds()[0]);
    }

    @Test
    void givenDefBudgetWithoutCategories_whenSave_thenPersistedInDBWithDefault() {
        final var defBudget = createDefBudget(true, true, true, true, false);

        final var saved = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(saved.getId());
        assertTrue(found.isPresent());
        assertEquals(1, found.get().getCategoryIds().length);
        assertEquals(0L, found.get().getCategoryIds()[0]);
    }

    @Test
    void givenDefBudgetWithoutAmount_whenSave_thenThrowException() {
        final var defBudget = createDefBudget(true, true, false, true, true);

        assertThrows(DataIntegrityViolationException.class,
                () -> defBudgetRepository.save(defBudget));
    }

    @Test
    void givenDefBudgetWithoutName_whenSave_thenThrowException() {
        final var defBudget = createDefBudget(false, true, true, true, true);

        assertThrows(DataIntegrityViolationException.class,
                () -> defBudgetRepository.save(defBudget));
    }

    @Test
    void givenDefBudgetWithoutPeriod_whenSave_thenThrowException() {
        final var defBudget = createDefBudget(true, false, true, true, true);

        final var saved = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(saved.getId());
        assertTrue(found.isPresent());
        assertEquals(0, found.get().getPeriod());
    }

    @Test
    void givenDefBudgetWithChangedName_whenUpdate_thenPersistedOkInDB() {
        final var defBudget = createDefBudget();
        defBudgetRepository.save(defBudget);
        defBudget.setName("Updated name");

        final var updated = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(updated.getId());
        assertTrue(found.isPresent());
        assertEquals(defBudget.getName(), found.get().getName());
    }

    @Test
    void givenDefBudgetWithChangedPeriod_whenUpdate_thenPersistedOkInDB() {
        final var defBudget = createDefBudget();
        defBudgetRepository.save(defBudget);
        defBudget.setPeriod(1);

        final var updated = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(updated.getId());
        assertTrue(found.isPresent());
        assertEquals(defBudget.getPeriod(), found.get().getPeriod());
    }

    @Test
    void givenDefBudgetWithChangedAmount_whenUpdate_thenPersistedOkInDB() {
        final var defBudget = createDefBudget();
        defBudgetRepository.save(defBudget);
        defBudget.setAmount(new BigDecimal("1000"));

        final var updated = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(updated.getId());
        assertTrue(found.isPresent());
        assertEquals(defBudget.getPeriod(), found.get().getPeriod());
    }

    @Test
    void givenDefBudgetWithAccountIds_whenUpdate_thenPersistedOkInDB() {
        final var defBudget = createDefBudget();
        defBudgetRepository.save(defBudget);
        defBudget.setAccountIds(new Long[]{1L, 3L, 5L, 7L});

        final var updated = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(updated.getId());
        assertTrue(found.isPresent());
        assertEquals(defBudget.getPeriod(), found.get().getPeriod());
    }

    @Test
    void givenDefBudgetWithCategoryIds_whenUpdate_thenPersistedOkInDB() {
        final var defBudget = createDefBudget();
        defBudgetRepository.save(defBudget);
        defBudget.setCategoryIds(new Long[]{2L, 4L, 6L, 8L});

        final var updated = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(updated.getId());
        assertTrue(found.isPresent());
        assertEquals(defBudget.getPeriod(), found.get().getPeriod());
    }

    @Test
    void givenDefBudgetWithVersion_whenUpdate_thenPersistedOkInDBWithIncrementedVersion() {
        final var defBudget = createDefBudget();
        defBudgetRepository.save(defBudget);
        defBudget.setName("Changed version to 2");

        final var updated = defBudgetRepository.save(defBudget);

        final var found = defBudgetRepository.findById(updated.getId());
        assertTrue(found.isPresent());
        assertEquals(2L, found.get().getVersion());
    }

    @Test
    void givenDefBudgetWithVersion1_whenUpdate_thenPersistedOkInDBWithVersion3() {
        final var defBudget = createDefBudget();
        defBudgetRepository.save(defBudget);

        final DefBudget updated = updateTwice(defBudget);

        final var found = defBudgetRepository.findById(updated.getId());
        assertTrue(found.isPresent());
        assertEquals(3L, found.get().getVersion());
    }



    private DefBudget updateTwice(DefBudget defBudget) {
        defBudget.setName("Changed version to 2");
        defBudgetRepository.save(defBudget);

        final var found1 = defBudgetRepository.findById(defBudget.getId());
        assertTrue(found1.isPresent());
        assertEquals(2L, found1.get().getVersion());

        final var defBudget1 = found1.get();
        defBudget1.setName("Changed version to 3");
        return defBudgetRepository.save(defBudget1);
    }
}
