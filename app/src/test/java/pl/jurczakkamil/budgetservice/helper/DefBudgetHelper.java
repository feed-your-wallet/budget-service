package pl.jurczakkamil.budgetservice.helper;

import pl.jurczakkamil.budgetservice.DefBudget;
import pl.jurczakkamil.budgetservice.DefBudgetDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class DefBudgetHelper {

    public static DefBudget createDefBudget() {
        final var defBudget = new DefBudget();
        defBudget.setName("Test Budget");
        defBudget.setPeriod(3);
        defBudget.setAmount(new BigDecimal("2000.00"));
        defBudget.setAccountIds(new Long[]{1L, 3L, 5L});
        defBudget.setCategoryIds(new Long[]{2L, 4L, 6L});

        return defBudget;
    }

    public static DefBudget createDefBudget(boolean withName, boolean withPeriod, boolean withAmount,
                                                            boolean withAccounts, boolean withCategories) {
        final var defBudget = new DefBudget();
        if (withName) defBudget.setName("Test Budget");
        if (withPeriod) defBudget.setPeriod(3);
        if (withAmount) defBudget.setAmount(new BigDecimal("2000.00"));
        if (withAccounts) defBudget.setAccountIds(new Long[]{2L, 4L, 6L});
        if (withCategories) defBudget.setCategoryIds(new Long[]{1L, 3L, 5L});

        return defBudget;
    }

    public static List<DefBudget> createDefBudgets(int count) {
        final var defBudgets = new ArrayList<DefBudget>();
        IntStream.range(0, count)
                .forEach(i -> defBudgets.add(createDefBudget()));
        return defBudgets;
    }

    public static DefBudgetDto createDefBudgetDto() {
        final var defBudgetDto = new DefBudgetDto();
        defBudgetDto.setName("Test Budget");
        defBudgetDto.setPeriod(3);
        defBudgetDto.setAmount(new BigDecimal("2000.00"));
        defBudgetDto.setAccountIds(new Long[]{1L, 3L, 5L});
        defBudgetDto.setCategoryIds(new Long[]{2L, 4L, 6L});

        return defBudgetDto;
    }

    public static List<DefBudgetDto> createDefBudgetDtos(int count) {
        return createDefBudgetDtos(count, false);
    }
    public static List<DefBudgetDto> createDefBudgetDtos(int count, boolean withIds) {
        final var dtos = new ArrayList<DefBudgetDto>();
        IntStream.range(0, count)
                .forEach(i -> createDefBudgetDto(withIds, dtos));
        return dtos;
    }

    public static boolean allFieldsMatched(DefBudgetDto o1, DefBudgetDto o2) {
        if (!o1.getName().equals(o2.getName())) return false;
        if (o1.getPeriod() != o2.getPeriod()) return false;
        if (!o1.getAmount().equals(o2.getAmount())) return false;

        final var accountIds1 = Arrays.asList(o1.getAccountIds());
        final var accountIds2 = Arrays.asList(o2.getAccountIds());
        if (!accountIds1.containsAll(accountIds2)) return false;

        final var categoryIds1 = Arrays.asList(o1.getCategoryIds());
        final var categoryIds2 = Arrays.asList(o2.getCategoryIds());
        return categoryIds1.containsAll(categoryIds2);
    }

    private static void createDefBudgetDto(boolean withIds, ArrayList<DefBudgetDto> dtos) {
        final var defBudgetDto = createDefBudgetDto();
        enrichWithIdAndVersion(withIds, defBudgetDto);
        dtos.add(defBudgetDto);
    }

    private static void enrichWithIdAndVersion(boolean withIds, DefBudgetDto defBudgetDto) {
        if (withIds) {
            final var id = Math.round(Math.random());
            defBudgetDto.setId(id);
            defBudgetDto.setVersion(1L);
        }
    }
}
