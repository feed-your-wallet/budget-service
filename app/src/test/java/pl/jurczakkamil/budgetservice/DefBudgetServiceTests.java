package pl.jurczakkamil.budgetservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.jurczakkamil.budgetservice.mapper.DefBudgetMapper;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static pl.jurczakkamil.budgetservice.helper.DefBudgetHelper.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class DefBudgetServiceTests {

    @Autowired
    private DefBudgetService defBudgetService;

    @Autowired
    private DefBudgetMapper defBudgetMapper;

    @Test
    void contextLoads() {
        assertNotNull(defBudgetService);
    }

    @Test
    void givenDto_whenSave_thenPersistInDbAndReturnMappedDto() {
        final var defBudgetDto = createDefBudgetDto();

        final var saved = defBudgetService.save(defBudgetDto);

        assertNotNull(saved.getId());
        assertNotNull(saved.getVersion());
        assertTrue(allFieldsMatched(defBudgetDto, saved));
    }

    @Test
    void givenId_whenFindById_thenReturnDto() {
        final var defBudgetDto = createDefBudgetDto();
        final var saved = defBudgetService.save(defBudgetDto);
        final var id = saved.getId();

        final var found = defBudgetService.findById(id);

        assertTrue(found.isPresent());
        assertTrue(allFieldsMatched(defBudgetDto, found.get()));
    }

    @Test
    void givenNullId_whenFindById_thenReturnEmptyOptional() {
        assertThrows(NullPointerException.class,
                () -> defBudgetService.findById(null),
                "ID cannot be null");
    }

    @Test
    void givenDefBudgetsIds_whenSumAmounts_thenReturnedCorrectSum() {
        final var defBudgets = createDefBudgets(5);
        final var mockDefBudgetRepository = mock(DefBudgetRepository.class);
        when(mockDefBudgetRepository.findAllById(anyList())).thenAnswer(e -> defBudgets);
        final var defBudgetService = new DefBudgetService(mockDefBudgetRepository, defBudgetMapper);

        final var ids = defBudgets.stream()
                .map(BaseEntity::getId)
                .toList();

        final var sum = defBudgetService.sumAmountsById(ids);

        assertEquals(new BigDecimal("10000.00"), sum);
    }
}
